// Завдання 1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
let q1 = Array.from(new Set([...clients1, ...clients2]));
console.log(q1);
 
//Завдання 2
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];
  class Users {
    constructor(name , lastName , age){
        this.name = name ;
        this.lastName = lastName;
        this.age = age;
    }
  }


  function aboba (arr) {
    let w =[];
    arr.forEach((elem )=> {
        let biba = new Users (elem.name , elem.lastName , elem.age );
        w.push(biba);

    });
    return w
  };
  let e = aboba(characters )
  console.log(e);


  //Завдання 3
  const user1 = {
    name: "John",
    years: 30
  };
  const {name ,  years , Admin = false} = user1
  console.log(name);
  console.log( years);
  console.log(Admin);
  console.log(`User1 :` , user1);



//Завдання 4


  const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

  const mainSatishi = {...satoshi2018 , ...satoshi2019 , ...satoshi2020};
  console.log(mainSatishi);


//Завдання 5




  const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin 11111111111111111111111111111111111'
  }
const d1 = [ ...books, bookToAdd ,];
console.log(d1);


//Завдання 6


const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const employeePlus = {...employee , 
  age: 31 ,
  salary: 1200,
}

console.log(employeePlus);

//Завдання 7


const array = ['value', () => 'showValue'];
[value, showValue] = array
alert(value); 
alert(showValue())


